################################################################################
#
# openwrt-mtd
#
################################################################################

OPENWRT_MTD_SITE = $(TOPDIR)/package/openwrt-mtd/openwrt-mtd.tar.gz
OPENWRT_MTD_SITE_METHOD = file

define OPENWRT_MTD_EXTRACT_CMDS
	echo $(OPENWRT_MTD_SITE)
	tar -zxf $(OPENWRT_MTD_SITE) -C $(@D)/ --strip-components=1
endef

define OPENWRT_MTD_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) CC=$(TARGET_CC) -C $(@D)
endef

define OPENWRT_MTD_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/mtd $(TARGET_DIR)/usr/bin/mtd
endef

$(eval $(generic-package))
