#! /bin/bash

file_name="keep-nmcli.sh"
gen_file="./$file_name"

gen_single_nmcli()
{
	# $1 name $2 ip4/24
	connection_name=$1"-connection"

	echo "exist=\$(nmcli c show | grep $connection_name | wc -l)" >> $gen_file
	echo "if [ \$exist -ne 1  ]; then" >> $gen_file
	echo "	nmcli c add type ethernet con-name $connection_name ifname $1 ip4 $2/24" >> $gen_file
	echo "fi" >> $gen_file
	echo "" >> $gen_file
}

gen_all()
{
	single_content=$(echo $1 | grep ' ')
	single_param=0
	if [[ -z "$single_content" ]]; then
		single_param=1
	fi

	loopp=1
	index=1
	while [ $loopp -eq 1 ];
	do
		ethname=$(echo $1 | cut -d ' ' -f$index)
		ethip=$(echo $2 | cut -d ' ' -f$index)
		if [[ -z $ethname ]]; then
			loopp=0
		elif [[ -z $ethip ]]; then
			loopp=0
		else
			echo "$ethname <======> $ethip"
			gen_single_nmcli $ethname $ethip
			index=$(($index + 1))
		fi
		if [ $single_param -eq 1 ]; then
			break;
		fi
	done
}

if [ $# -ne 2 ]; then
	echo "param not full! exit 0"
	exit 0
fi

echo "#! /bin/sh" > $gen_file
echo "sleep 2" >> $gen_file
echo "" >> $gen_file

gen_all "$1" "$2"

echo "systemctl disable keep-nmcli" >> $gen_file
echo "rm /usr/lib/systemd/system/multi-user.target.wants/keep-nmcli.service" >> $gen_file
echo "rm /usr/lib/systemd/system/keep-nmcli.service" >> $gen_file
echo "rm /root/$file_name" >> $gen_file
chmod a+x $gen_file

