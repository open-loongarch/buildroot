################################################################################
#
# swconfig
#
################################################################################

SWCONFIG_SITE = $(TOPDIR)/package/swconfig/swconfig.tar.gz
SWCONFIG_SITE_METHOD = file

define SWCONFIG_EXTRACT_CMDS
	echo $(SWCONFIF_SITE)
	tar -zxf $(SWCONFIG_SITE) -C $(@D)/
endef

define SWCONFIG_INSTALL_STAGING_CMDS
	echo "nothing to do"
endef

define SWCONFIG_INSTALL_TARGET_CMDS
	echo "install swconfig"
	mkdir -p $(TARGET_DIR)/usr/bin
	cd $(@D) && chmod a+x ./swconfig && cp swconfig $(TARGET_DIR)/usr/bin
endef

$(eval $(generic-package))
