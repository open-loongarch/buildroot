#!/bin/sh
#

FLAG=/etc/init.d/resizerootfs

ROOTDEV_P=`cat /proc/cmdline  | sed 's/ /\n/g' | grep root= | cut -d = -f 2`
ROOTDEV="/dev/"
PARTNUM=0

# /dev/sdax
choose_ssd_root(){
	ROOTDEV=`echo ${ROOTDEV_P} | sed 's/[1-9]//g'`
	if [[ "${ROOTDEV_P}" == "${ROOTDEV}" ]]; then
		return
	else
		PARTNUM=`echo ${ROOTDEV_P#${ROOTDEV}}`
	fi
}

# /dev/mmcblkxpx
choose_mmc_root(){
	ROOTDEV=`echo ${ROOTDEV_P} | cut -d p -f 1`
	if [[ "${ROOTDEV_P}" == "${ROOTDEV}" ]]; then
		return
	else
		PARTNUM=`echo ${ROOTDEV_P} | cut -d p -f 2`
	fi
}

do_resize(){
	if [[ ${PARTNUM} == 0 ]]; then
		echo "Don't resize partition"
	else
		echo "Partition resizing..."
		parted ${ROOTDEV} --script -- resizepart ${PARTNUM} -0
	fi

	resize2fs ${ROOTDEV_P}
	echo 1 > $FLAG
}

if [ -f ${FLAG} ]; then
	echo "Rootfs Resized."
else
	echo "Rootfs Resizing..."
	if [[ ${ROOTDEV_P} == *"/dev/sd"* ]]; then
		choose_ssd_root
		do_resize
	elif [[ ${ROOTDEV_P} == *"/dev/mmcblk"* ]]; then
		choose_mmc_root
		do_resize
	else
		echo "${ROOTDEV_P} cannot resize"
	fi
fi

