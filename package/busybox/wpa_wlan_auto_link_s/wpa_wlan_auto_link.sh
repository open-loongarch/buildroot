#! /bin/sh

default_user="wifi-test"
default_passwd="wifi-test123"
default_wlan_dev="wlan0"

wpa_config="/etc/wpa_supplicant.config"

#sleep 5

if [ ! -f $wpa_config ]; then
	wpa_passphrase $default_user $default_passwd > $wpa_config
fi

wpa_supplicant -B -i $default_wlan_dev -c $wpa_config
dhcpcd $default_wlan_dev

echo "success run wpa wlan auto link service"

