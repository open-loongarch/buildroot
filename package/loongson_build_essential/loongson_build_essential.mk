################################################################################
#
# loongson_build_essential
#
################################################################################

ifeq ($(BR2_PACKAGE_LOONGSON_BUILD_ESSENTIAL_CMAKE),y)
LOONGSON_BUILD_ESSENTIAL_SITE = $(TOPDIR)/package/loongson_build_essential/loongson-build-essential-cmake.tar.gz
else ifeq ($(BR2_PACKAGE_LOONGSON_BUILD_ESSENTIAL_CMAKE_GDB),y)
LOONGSON_BUILD_ESSENTIAL_SITE = $(TOPDIR)/package/loongson_build_essential/loongson-build-essential-cmake-gdb.tar.gz
else
LOONGSON_BUILD_ESSENTIAL_SITE = $(TOPDIR)/package/loongson_build_essential/loongson-build-essential.tar.gz
endif
LOONGSON_BUILD_ESSENTIAL_SITE_METHOD = file

define LOONGSON_BUILD_ESSENTIAL_EXTRACT_CMDS
	echo "extract $(LOONGSON_BUILD_ESSENTIAL_SITE)"
	tar -xzf $(LOONGSON_BUILD_ESSENTIAL_SITE) -C $(@D)/
endef

define LOONGSON_BUILD_ESSENTIAL_INSTALL_TARGET_CMDS
	echo "do nothing"
endef

$(eval $(generic-package))
